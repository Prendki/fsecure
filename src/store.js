import Vue from 'vue'
import Vuex from 'vuex'
import tasks from './modules/store/tasks';

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        tasks
    }
})
