export const notBlank = (value) => {
    return /^[a-zA-Z]/.test(value)
}
