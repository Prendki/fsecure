import axios from "axios";
import TaskStatuses from "../../enums/TaskStatuses";
import Vue from "vue";

const state = {
    tasks: [],
    orderBy: "id",
    target: "ASC",
}

// getters
const getters = {
    getTasks(state) {
        return state.tasks;
    },
    getTasksForDelete(state) {
      return state.tasks.filter((item) => {
          return item.checked ===true
      })
    },

    getFilteredTasks(state) {
        return state.tasks.sort((a, b) => {
            return (state.target === "ASC" ? 1 : -1) * ('' + a[state.orderBy]).localeCompare(b[state.orderBy]);
        })
    },
    getSortedByIdTasks(state) {
        return state.tasks.sort((a, b) => {
            return ('' + a['id']).localeCompare(b['id']);
        })
    },
    getTaskByStatus: (state) => (status) => {
        return state.tasks.filter((item) => {
            return item.status === TaskStatuses[status];
        });
    },
    getOrderBy(state) {
        return state.orderBy;
    },
    getTarget(state) {
        return state.target;
    }
}
// actions
const actions = {
    loadTasks: ({commit}) => {
        return new Promise((resolve, reject) => {
            axios.get("/example-object.json").then((response) => {
                commit("setTasks", response.data)
                resolve(response.data);
            }).catch((error) => {
                reject(error);
            });
        })
    },
    addTask: ({commit, state, getters},task ) => {
        return new Promise((resolve) => {
            //Here should be request at all ... but ... whatever...

            // Modern Problems Require Modern Solutions
            const tasks = getters['getSortedByIdTasks'];
            const last_task = tasks[tasks.length-1]
            const new_id =  (last_task ? last_task.id +1 : 1);

            // What ? I have no API... You saw nothing...
            task.id = new_id;
            task.status = TaskStatuses['STATUS_NEW']

            commit("addTask", task)
            resolve(state.tasks);
        })
    }
}

// mutations
const mutations = {
    setTasks(state, tasks = []) {
        state.tasks = tasks
    },
    setTaskForDelete(state, task) {
        task.checked = !task.checked;
        Vue.set(state.tasks, state.tasks.findIndex(item => item === task), task)
    },
    setTasksForDelete(state, checkedAll) {
        const tasks = state.tasks
        tasks.forEach((item,index) => {
            item.checked = checkedAll
            Vue.set(state.tasks,index,item);
        });

        state.tasks = tasks;
    },
    removeTasksForDelete(state) {
        state.tasks.forEach((item) => {
            delete item.checked;
        })
    },
    removeTasks(state) {
      state.tasks = state.tasks.filter((item) => {
          return item.checked !== true
      })
    },
    setOrderBy(state, orderBy = "") {
        if (state.orderBy === orderBy)
            state.target = (state.target === "ASC" ? "DESC" : "ASC")
        else
            state.target = "ASC"

        state.orderBy = orderBy
    },
    setTarget(state, target = "") {
        state.target = target
    },
    addTask(state, task) {
        state.tasks.push(task);
    }
}


export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
