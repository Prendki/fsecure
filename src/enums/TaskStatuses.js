export default {
    STATUS_NEW : "New",
    STATUS_COMPLETED : "Completed",
    STATUS_NOT_COMPLETED : "Not completed"
}
